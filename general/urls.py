from django.conf.urls import url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from django.views.generic import TemplateView
from pages.views import UploadLayoutView, ConditionsView, ThanksView

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', TemplateView.as_view(template_name='main.html')),
    url(r'^send$', UploadLayoutView.as_view()),
    url(r'^rules$', ConditionsView.as_view()),
    url(r'^thanks$', ThanksView.as_view())
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
