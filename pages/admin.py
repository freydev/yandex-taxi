from django.contrib import admin
from pages.models import CompetitionEntry

admin.site.register(CompetitionEntry)
