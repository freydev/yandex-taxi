#! coding: utf8
from __future__ import unicode_literals

from django.apps import AppConfig


class PagesConfig(AppConfig):
    name = 'pages'
    verbose_name = u'ПРИСЛАННЫЕ МАКЕТЫ'
