#! coding: utf8

from django import forms

class LayoutForm(forms.Form):
    work_name = forms.CharField(max_length=100, label=u'Название работы')
    email = forms.EmailField(label=u'Электронная почта')
    comment = forms.CharField(label=u'Описание работы', widget=forms.Textarea, required=False)
    texture_data = forms.CharField(widget=forms.HiddenInput(), label='')
