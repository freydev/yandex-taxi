#! coding: utf8

from __future__ import unicode_literals

from django.db import models


class CompetitionEntry(models.Model):
    name = models.CharField(u'Название работы', max_length=255)
    email = models.EmailField(u'Электронная почта')
    comment = models.TextField(u'Описание работы', max_length=255, blank=True)

    texture = models.FileField(u'Текстура', upload_to='uploads/')

    class Meta:
        verbose_name = u'Конкурсные работы'
        verbose_name_plural = u'Конкурсная работа'
