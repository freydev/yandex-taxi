function declOfNum(number, titles) {
    cases = [2, 0, 1, 1, 1, 2];
    return titles[ (number%100>4 && number%100<20)? 2 : cases[(number%10<5)?number%10:5] ];
}

var dragTimer;
$(document).on('dragover', function(e){
    e.preventDefault();
    e.stopPropagation();

    var dt = e.originalEvent.dataTransfer;
    if(dt.types != null && (dt.types.indexOf ? dt.types.indexOf('Files') != -1 : dt.types.contains('application/x-moz-file'))) {
        $('.viewport__dropzone').removeClass('hidden');
        clearTimeout(dragTimer);
    }
})

$(document).on('dragleave', function(e) {
    e.preventDefault();
    e.stopPropagation();

    dragTimer = setTimeout(function(){
        $('.viewport__dropzone').addClass('hidden');
    }, 25)
})

$(document).ready(function(){
    $('.viewport__blender').on('wheel', function(e){
        var oEvent = e.originalEvent,
            delta = oEvent.deltaY || oEvent.wheelDelta;

        window.scrollBy(0, delta)
    });

    $("#id_comment").on('keydown keyup', function(e){
        if ($(this).val().length > 280) {
            $(this).val($(this).val().substring(0, 280));
        }

        $(".js-remaining").find('.js-num').text(280 - $(this).val().length)
        $(".js-ending").text(declOfNum(280 - $(this).val().length, ['', 'а', 'ов']))
    })

    $('.js-submit').on('click', function(e){
        //validate

        e.preventDefault();
        if ($('form#upload_layout input[name="texture_data"]').val().length == 0) {
            $('#error_texture').addClass('form__error--show').text('Выберите макет');
            $('html body').stop().animate({scrollTop:0}, '500', 'swing');
            return
        }
        $('#error_texture').removeClass('form__error--show');

        if ($('form#upload_layout input[name="work_name"]').val().length == 0) {
            $('#error_work_name').addClass('form__error--show');
            return
        }
        $('#error_work_name').removeClass('form__error--show');

        if ($('form#upload_layout input[name="email"]').val().length == 0) {
            $('#error_email').addClass('form__error--show').text('Заполните это поле');
            return
        }
        $('#error_email').removeClass('form__error--show');

        email_regex = /^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i;
        if ($('form#upload_layout input[name="email"]').val().length > 0 &&
            !email_regex.test($('form#upload_layout input[name="email"]').val())) {
            $('#error_email').addClass('form__error--show').text('Неверный адрес email');
            return
        }
        $('#error_email').removeClass('form__error--show');


        if (!$('form#upload_layout input[name="accept"]').prop('checked')) {
            $('#error_accept').addClass('form__error--show');
            return
        }
        $('#error_accept').removeClass('form__error--show');

        $('form#upload_layout').submit();
    })
});


b4w.register('form_scene', function(exports, require) {
    var m_data = require('data'),
        m_app = require('app'),
        m_scenes = require('scenes'),
        m_textures = require('textures'),
        __m_textures = require('__textures'),
        m_cam = require('camera');

    function init_cb (){
        m_data.load('/static/scene/yataxi_form.json', load_cb, function(percent){
            document.querySelector('.scene__loader-text').innerHTML = percent
            if (percent == 100) {
                document.querySelector('.scene__loader').remove()
                document.querySelector('.scene__loader-text').remove()
            }
        })
    }

    function load_cb (){
        m_app.enable_camera_controls();

        $('.viewport__texture-input').on('change', function(){
            loadTexture(this.files[0])
        })
    }

    function loadTexture(file) {
        if (['image/png', 'image/jpeg'].indexOf(file.type) == -1) {
            $('#error_texture').addClass('form__error--show').text('Неверный формат макета');
            return
        } else if (file.size > (10 * 1024 * 1024)) {
            $('#error_texture').addClass('form__error--show').text('Файл не должен превышать 10МБ');
            return
        }

        $('#error_texture').removeClass('form__error--show');

        var korpus = m_scenes.get_object_by_name('Corpus');
        var fr = new FileReader();

        fr.onload = function(event){
            var buffer = fr.result;
            $('#id_texture_data').val(buffer)
            var img = document.createElement('img');
            img.src = buffer;
            img.onload = function(){
                __m_textures.change_image(korpus, 'carpaint', img);
            }
        };

        fr.readAsDataURL(file)
    }

    $('.viewport__dropzone').on('drop', function(e){
        $('.viewport__dropzone').addClass('hidden');

        e.preventDefault();
        e.stopPropagation();

        var file = e.originalEvent.dataTransfer.files[0];

        loadTexture(file);
    })

    exports.init = function(){
        m_app.init({
            canvas_container_id: "viewport__blender",
            autoresize: true,
            report_init_failure: true,
            callback: init_cb
        });
    }
});

var form_scene = b4w.require('form_scene'); form_scene.init();
