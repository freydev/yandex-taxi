"use strict";

b4w.register('main_scene', function (exports, require) {
    var m_app = require('app'),
        m_data = require('data'),
        m_scenes = require('scenes'),
        m_anim = require('animation'),
        m_trans = require('transform'),
        m_cam = require('camera'),
        m_cam_anim = require('camera_anim'),
        m_main = require('main'),
        m_textures = require('textures'),
        m_vec = require('vec3');

    window.m_cam = m_cam;
    window.m_cam_anim = m_cam_anim;
    window.m_scenes = m_scenes;

    $('.scene__description').removeClass('scene__description--progress')
        .css({left: '12%', top: '45%'});

    var ease = bezier(0.25, 0.10, 0.25, 1),
        camera, rotation_fov, static_fov = 1,
        requestID, animDuration = 30,
        rotation_translateX = 0,
        static_translateX = -2;

    var lastFov, lastTranslateX,
        isCentered, inCentering;

    function load_cb() {
        camera = m_scenes.get_active_camera();
        rotation_fov = m_cam.get_fov(camera);

        var korpus = m_scenes.get_object_by_name('Corpus');

        var currentTexture = 1;
        var change_texture = function() {
            if (currentTexture > 10) currentTexture = 1;
            m_textures.change_image(korpus, 'carpaint', '/static/img.assets/textures/' + currentTexture++ + '.jpg',
            function(){
                setTimeout(function(){
                    change_texture()
                }, 500)
            });
        };

        change_texture();

        m_cam.set_fov(camera, static_fov);
        m_cam.translate_view(camera, static_translateX, 0, 0)
        m_cam.rotate_camera(camera, 5.67, 0, true, true);
        m_app.enable_camera_controls(false, true, true);

        $('#scene__blend').on('mousedown', function (e) {
            if (inCentering) return;

            var fovRange = static_fov - rotation_fov;
            var translateRange = static_translateX - rotation_translateX,
                fov = m_cam.get_fov(camera),
                animTick = 0;

            $('.scene__plane').removeClass('scene__plane--show');
            $('.scene__art').addClass('scene__art--progress');
            $('.scene__description').addClass('scene__description--progress');

            (function draw() {

                if (animTick <= animDuration && !isCentered)
                    requestID = requestAnimationFrame(draw);
                else {
                    isCentered = true;
                    inCentering = false;
                    return;
                }

                inCentering = true;
                var percent = ease(animTick / animDuration) * 100;

                lastFov = static_fov - fovRange * percent / 100
                lastTranslateX = static_translateX - translateRange * percent / 100
                m_cam.set_fov(camera, lastFov);
                m_cam.translate_view(camera, lastTranslateX, 0, 0);
                animTick++;

                if (animTick == 30) {
                    $('.scene__plane, .scene__art, .scene__description').hide();
                }
            })()
        })
    }

    function init_cb() {
        m_data.load('/static/scene/yataxi.json', load_cb, function (percent) {
            document.querySelector('.scene__loader-text').innerHTML = percent
            if (percent == 100) {
                document.querySelector('.scene__loader').remove()
                document.querySelector('.scene__loader-text').remove()
                var hiddenEl = document.querySelector('.transparent');
                hiddenEl.style.opacity = 1;
            }
        });
    }

    exports.to_camera_static = function (angle, left, progress_cb) { // angle in radians
        var absolute_camera_angle = angle,
            angle = m_cam.get_camera_angles(camera)[0],
            phi_range = angle - absolute_camera_angle,
            fovRange = static_fov - rotation_fov,
            translateRange = 2,
            fov = m_cam.get_fov(camera),
            animTick = 0;

        $('.scene__plane, .scene__art, .scene__description').show();
        if (angle < absolute_camera_angle) {
            if (absolute_camera_angle - angle < Math.PI) phi_range = absolute_camera_angle - angle;
            else phi_range = absolute_camera_angle - Math.PI * 2 - angle
        } else {
            if (angle - absolute_camera_angle < Math.PI) phi_range = absolute_camera_angle - angle;
            else phi_range = Math.PI * 2 - angle + absolute_camera_angle
        }

        isCentered = false;

        (function draw() {
            if (animTick <= animDuration)
                requestID = requestAnimationFrame(draw);

            var percent = ease(animTick / animDuration) * 100;

            lastFov = rotation_fov + fovRange * percent / 100;
            lastTranslateX = (left ? -1 : 1) * (rotation_translateX - translateRange * percent / 100);

            static_translateX = lastTranslateX;
            m_cam.set_fov(camera, lastFov);
            m_cam.translate_view(camera, lastTranslateX, 0, 0);
            m_cam.rotate_camera(camera, angle + phi_range * percent / 100, 0, true);

            if (progress_cb) progress_cb(animTick);
            animTick++;
        })();
    };

    exports.init = function () {
        m_app.init({
            canvas_container_id: "scene__blend",
            autoresize: true,
            report_init_failure: true,
            callback: init_cb
        });
    };
});


var main_scene = b4w.require('main_scene');
main_scene.init();
document.querySelector('.scene__controls-find').onclick = function () {
    $('.scene__plane').removeClass('scene__plane--show');
    $('.scene__art').addClass('scene__art--progress');
    $('.scene__description').addClass('scene__description--progress');

    main_scene.to_camera_static(5.67, false, function (tick) {
        if (tick == 10) {
            $('.scene__plane').removeClass('scene__plane--2')
                .addClass('scene__plane--1');
        }
        if (tick == 20) {
            $('.scene__art').removeClass('scene__art--progress')
                .removeClass('scene__art--2')
                .addClass('scene__art--1')
            $('.scene__description').removeClass('scene__description--progress')
                .css({left: '12%', top: '45%'})
                .html('Музей уличного искусства и Яндекс.Такси ' +
                    'запускают открытый конкурс «Стрит-арт на борту». Участники должны раскрасить автомобиль — превратить его в арт-объект, элемент городской культуры. Работы 10 победителей появятся' +
                    ' на машинах Яндекс.Такси и будут курсировать по Петербургу в течение года.<br><br><a href="/rules">Правила участия</a>')
        }
        if (tick == 30) {
            $('.scene__plane').addClass('scene__plane--show')
        }
    });
}
document.querySelector('.scene__controls-express').onclick = function () {
    $('.scene__plane').removeClass('scene__plane--show');
    $('.scene__description').addClass('scene__description--progress');
    $('.scene__art').addClass('scene__art--progress');

    main_scene.to_camera_static(0.41, true, function (tick) {
        if (tick == 10) {
            $('.scene__plane').removeClass('scene__plane--1')
                .addClass('scene__plane--2')
        }
        if (tick == 20) {
            $('.scene__art').removeClass('scene__art--progress')
                .removeClass('scene__art--1')
                .addClass('scene__art--2')
            $('.scene__description').removeClass('scene__description--progress')
                .css({left: '57%', top: '50%'})
                .html('Чтобы участвовать в конкурсе, нужно<br>скачать шаблон, создать по нему свой эскиз<br> и приложить его к заявке.<a href="/static/yataxi_pattern_mockup.psd" class="scene__controls-download">Скачать шаблон</a><a href="/send" class="scene__controls-layout">Отправить работу</a>')
        }
        if (tick == 30) {
            $('.scene__plane').addClass('scene__plane--show')
        }
    });
}
