from django.views.generic import TemplateView
from django.views.generic.base import ContextMixin
from django.shortcuts import redirect
from pages.forms import LayoutForm
from pages.models import CompetitionEntry
from django.core.files.base import ContentFile
import base64

class UploadLayoutView(TemplateView):
    template_name = 'upload_layout.html'

    def get_context_data(self, **kwargs):
        context = super(UploadLayoutView, self).get_context_data(**kwargs)
        context['layout_form'] = LayoutForm()
        return context

    def post(self, request, *args, **kwargs):
        form = LayoutForm(request.POST)
        if form.is_valid():
            work_name = form.cleaned_data['work_name']
            email = form.cleaned_data['email']
            comment = form.cleaned_data['comment']
            texture_data = form.cleaned_data['texture_data']

            entry = CompetitionEntry(
                name=work_name,
                email=email,
                comment=comment)

            entry.texture = ContentFile(base64.b64decode(texture_data), 'texture.jpg')
            entry.save()

            return redirect('/thanks')

class ConditionsView(TemplateView):
    template_name = 'conditions.html'

class ThanksView(TemplateView):
    template_name = 'thanks.html'
